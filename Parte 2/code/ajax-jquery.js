//simplificado
$(document).ready((event)=>{
    //Aquí nuesto código
    $.ajax('https://jsonplaceholder.typicode.com/photos',{
        method: 'GET',
    }).done((data)=>{
       //Codigo de exito
       let out = '';

       $(data).each((index, element)=>{
            out += `
                <div class="images"><img src="${element.thumbnailUrl}" /></div>
            `; 
            if(index > 21)
                return false;
       });

       $('#photos').html(out)
    }).fail(()=>{
       console.error("Ha ocurrido un problema al cargar la pagina");
    });
    
    $('#registro button').on('click',(event)=>{
        event.preventDefault();
        
        let titulo = $('#titulo').val();
        let mensaje = $('#mensaje').val();
        let idusuario = $('#idusuario').val();

        let body = JSON.stringify({
            title: titulo,
            body: mensaje,
            userId: idusuario
        });
        
        $.ajax('https://jsonplaceholder.typicode.com/photos',{
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            data: body,
            beforeSend: (()=> {
                $('#btn').attr('disabled', true);
                $('#btn span').hide();
                $('#spinner').fadeIn(300);
            })
        }).done(()=>{
            $('.alert').text('Usuario registrado exitosamente')
            .css('background', '#92ffbe').show()
            .fadeOut(4000);
        }).fail(()=>{
            $('.alert').text('Disculpe!! ha ocurrido un error en el registro')
            .css('background', '#ff9595').show()
            .fadeOut(4000);
        }).always(()=>{
            $('#spinner').hide();
            $('#btn span').fadeIn(300);
        });

    });

});



/*$.ajax('https://jsonplaceholder.typicode.com/photos',{

    method: 'GET', //Debes usar type si usa versiones de jQuery anteriores a 1.9.0.
    headers: {},
    data: {},
    dataType: 'JSON',
    statusCode: {
        404: function() {
            alert( "page not found" );
        }
    },
    xhr: (()=>{

    }), 
    beforeSend: (()=>{

    }), 

}).done(()=>{

}).fail(()=>{

}).always(()=>{

});*/
