
http_request = new XMLHttpRequest();                //Instancia del objeto XMLHttpRequest


//Utilizando funcion de flecha
http_request.onreadystatechange = (() => {   //Funcion que manejara la respuesta
    
    if (http_request.readyState == 4) {
        
        if (http_request.status == 200) {
            console.log('Proceso completado Exitosamente');
            
            let PostArray = JSON.parse(http_request.responseText); //almacenamos nuestro array en una variable para simplificar su usp
            let out = '';

            PostArray.forEach(post => { //recorremos nuestro array y almacenamos el valor en la variable out por cada ciclo
                out += `
                    <div>
                        <h3>${post.id}) ${post.title}</h3>
                        <p>${post.body}</p>
                    </div>
                `;
            });

            document.getElementById("posts").innerHTML = out; //agregamos todo nuestra estructura al DOM

        } else {
            console.error('hubo algún problema con la petición');
        }

    } else {
        console.log('procesando');
    }

});  

http_request.open('GET', 'https://jsonplaceholder.typicode.com/posts', true);
http_request.send();